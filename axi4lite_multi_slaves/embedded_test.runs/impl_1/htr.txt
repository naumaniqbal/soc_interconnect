#
# Vivado(TM)
# htr.txt: a Vivado-generated description of how-to-repeat the
#          the basic steps of a run.  Note that runme.bat/sh needs
#          to be invoked for Vivado to track run status.
# Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
#

vivado -log axi4lite_multi_slave_top.vdi -applog -m64 -product Vivado -messageDb vivado.pb -mode batch -source axi4lite_multi_slave_top.tcl -notrace
