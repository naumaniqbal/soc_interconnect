################################################################################
## Auto-Generated Script File, Do Not Modify
################################################################################
create_project /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/build/vivado_2016.3/embedded_test/embedded_test.xpr -force -part xcku040-ffva1156-2-e
set_property part xcku040-ffva1156-2-e [current_project]
set_property target_language VHDL [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_FIFO XPM_MEMORY} [current_project]
################################################################################
## Adding Enabled IP from Settings File:
##    /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/settings/default_settings.yml
################################################################################
################################################################################
## Adding Top-Level Constraints from Settings File:
################################################################################
################################################################################
## Adding Source Files Derived from: embedded_test_lib.axi4lite_multi_slave_top
################################################################################
add_files -norecurse "/local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl_packages/embedded_test_pkg.vhd"
set_property library embedded_test_lib [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl_packages/embedded_test_pkg.vhd]
set_property FILE_TYPE {VHDL 2008} [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl_packages/embedded_test_pkg.vhd]
add_files -norecurse "/local/users/joc92375/local_svn_working_copy/libraries/lib_ox/common_ox/src/vhdl_packages/ox_functions_pkg.vhd"
set_property library common_ox_lib [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/common_ox/src/vhdl_packages/ox_functions_pkg.vhd]
set_property FILE_TYPE {VHDL 2008} [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/common_ox/src/vhdl_packages/ox_functions_pkg.vhd]
add_files -norecurse "/local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4s_pkg.vhd"
set_property library axi4_lib [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4s_pkg.vhd]
set_property FILE_TYPE {VHDL 2008} [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4s_pkg.vhd]
add_files -norecurse "/local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4lite_pkg.vhd"
set_property library axi4_lib [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4lite_pkg.vhd]
set_property FILE_TYPE {VHDL 2008} [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_ox/axi4/src/vhdl_packages/axi4lite_pkg.vhd]
add_files -norecurse "/local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl/axi4lite_multi_slave_top.vhd"
set_property library embedded_test_lib [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl/axi4lite_multi_slave_top.vhd]
set_property FILE_TYPE {VHDL 2008} [get_files /local/users/joc92375/local_svn_working_copy/libraries/lib_stfc/embedded_test/src/vhdl/axi4lite_multi_slave_top.vhd]
quit
