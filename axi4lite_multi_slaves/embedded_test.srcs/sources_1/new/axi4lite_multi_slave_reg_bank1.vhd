library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;
library work;
use work.axi4lite_multi_slave_reg_bank1_pkg.all;
     
entity axi4lite_multi_slave_reg_bank1 is
   port(
      axi4lite_aclk : in std_logic;
      axi4lite_aresetn : in std_logic;
      
      axi4lite_mosi : in t_axi4lite_mosi;
      axi4lite_miso : out t_axi4lite_miso

--      axi4lite_emb_packet_generator_in_we : in t_axi4lite_multi_slave_reg_bank0_decoded;
--      axi4lite_multi_slave_reg_bank0_in : in t_axi4lite_multi_slave_reg_bank0;
--      axi4lite_multi_slave_reg_bank0_out_we : out t_axi4lite_multi_slave_reg_bank0_decoded;
--      axi4lite_multi_slave_reg_bank0_out : out t_axi4lite_multi_slave_reg_bank0
   );
end entity;     

architecture axi4lite_multi_slave_reg_bank1_a of axi4lite_multi_slave_reg_bank1 is 

   signal ipb_mosi : t_ipb_mosi;
   signal ipb_miso : t_ipb_miso;
   
   signal ipb_mosi_arr : t_ipb_multi_slave_reg_bank1_mosi_arr;
   signal ipb_miso_arr : t_ipb_multi_slave_reg_bank1_miso_arr;
   
   signal axi4lite_multi_slave_reg_bank1_int_we : t_axi4lite_multi_slave_reg_bank1_decoded;
   signal axi4lite_multi_slave_reg_bank1_int_re : t_axi4lite_multi_slave_reg_bank1_decoded;
   signal axi4lite_multi_slave_reg_bank1_int : t_axi4lite_multi_slave_reg_bank1;

begin
   --
   --
   --
   axi4lite_slave_logic_inst: entity axi4_lib.axi4lite_slave_logic
   port map (
      axi4lite_aclk => axi4lite_aclk,
      axi4lite_aresetn => axi4lite_aresetn,
      axi4lite_mosi => axi4lite_mosi,
      axi4lite_miso => axi4lite_miso,
      ipb_mosi => ipb_mosi,
      ipb_miso => ipb_miso
   );
   --
   -- blocks_muxdemux
   --
   axi4lite_multi_slave_reg_bank1_muxdemux_inst: entity work.axi4lite_multi_slave_reg_bank1_muxdemux
   port map(
      axi4lite_aclk => axi4lite_aclk,
      axi4lite_aresetn => axi4lite_aresetn,
      ipb_mosi => ipb_mosi,
      ipb_miso => ipb_miso,
      ipb_mosi_arr => ipb_mosi_arr,
      ipb_miso_arr => ipb_miso_arr   
   );

   --
   -- Address decoder
   --
   axi4lite_multi_slave_reg_bank1_int_we <= axi4lite_multi_slave_reg_bank1_full_decoder(ipb_mosi_arr(0).addr,ipb_mosi_arr(0).wreq);
   axi4lite_multi_slave_reg_bank1_int_re <= axi4lite_multi_slave_reg_bank1_full_decoder(ipb_mosi_arr(0).addr,ipb_mosi_arr(0).rreq);
   --
   -- Register write process
   --
   process(axi4lite_aclk,axi4lite_aresetn)
   begin
      if rising_edge(axi4lite_aclk) then
--         axi4lite_multi_slave_reg_bank1_out_we <= axi4lite_multi_slave_reg_bank1_int_we;
         --
         -- Write to registers from logic, put assignments here 
         -- if logic has lower priority than axi4lite bus master 
         --
         -- ...
         --
         -- hw_permission="w" or hw_permission="wen"
         -- hw_prio="bus"
         --

         --====================================================================
         --
         -- Write to registers from axi4lite side, think twice before modifying
         --
         axi4lite_multi_slave_reg_bank1_write_reg(ipb_mosi_arr(0).wdat,
                                      axi4lite_multi_slave_reg_bank1_int_we,
                                      axi4lite_multi_slave_reg_bank1_int);
         --
         --====================================================================
         --
         -- Write to registers from logic, put assignments here 
         -- if logic has higher priority than axi4lite bus master
         --
         -- ...
         --
         -- hw_permission="w" or hw_permission="wen"
         -- hw_prio="logic"
         --
--         axi4lite_multi_slave_reg_bank1_int.packet_count_lower <= axi4lite_multi_slave_reg_bank1_in.packet_count_lower;
--         axi4lite_multi_slave_reg_bank1_int.packet_count_upper <= axi4lite_multi_slave_reg_bank1_in.packet_count_upper;
--         axi4lite_multi_slave_reg_bank1_int.word_count_lower <= axi4lite_multi_slave_reg_bank1_in.word_count_lower;
--         axi4lite_multi_slave_reg_bank1_int.word_count_upper <= axi4lite_multi_slave_reg_bank1_in.word_count_upper;

      end if;
      if axi4lite_aresetn = '0' then
         axi4lite_multi_slave_reg_bank1_reset(axi4lite_multi_slave_reg_bank1_int);
      end if;
   end process;
   
   ipb_miso_arr(0).wack <= '1';
   ipb_miso_arr(0).rack <= '1';
   ipb_miso_arr(0).rdat <= axi4lite_multi_slave_reg_bank1_read_reg(axi4lite_multi_slave_reg_bank1_int_re,
                                                       axi4lite_multi_slave_reg_bank1_int);

--   axi4lite_multi_slave_reg_bank1_out    <= axi4lite_multi_slave_reg_bank1_int; 
   
   
end architecture;

