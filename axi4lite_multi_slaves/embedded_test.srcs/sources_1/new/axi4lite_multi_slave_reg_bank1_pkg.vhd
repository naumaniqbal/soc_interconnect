library ieee;
use ieee.std_logic_1164.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;

package axi4lite_multi_slave_reg_bank1_pkg is 

   --##########################################################################
   --
   -- Register Records
   --
   --##########################################################################
   type t_axi4lite_multi_slave_reg_bank1_control is record
      test_gen_select: std_logic;
      trigger: std_logic;
      max_count_enable: std_logic;
      enable: std_logic;
      packet_select: std_logic_vector(3 downto 0);
      walking: std_logic;
      soft_reset: std_logic;
   end record;

   type t_axi4lite_multi_slave_reg_bank1 is record
      reg0: std_logic_vector(31 downto 0);
      reg1: std_logic_vector(31 downto 0);
      reg2: std_logic_vector(31 downto 0);
      reg3: std_logic_vector(31 downto 0);
      
   end record;

   --##########################################################################
   --
   -- Register Decoded Records
   --
   --##########################################################################
   type t_axi4lite_multi_slave_reg_bank1_control_decoded is record
      test_gen_select: std_logic;
      trigger: std_logic;
      max_count_enable: std_logic;
      enable: std_logic;
      packet_select: std_logic;
      walking: std_logic;
      soft_reset: std_logic;
   end record;

   type t_axi4lite_multi_slave_reg_bank1_decoded is record
      reg0: std_logic;
      reg1: std_logic;
      reg2: std_logic;
      reg3: std_logic;
   end record;

   --##########################################################################
   --
   -- Register Descriptors
   --
   --##########################################################################
   type t_access_type is (r,w,rw);
   type t_reset_type is (async_reset,no_reset);
   
   type t_reg_descr is record
      offset: std_logic_vector(31 downto 0);
      bit_hi: natural;
      bit_lo: natural;
      rst_val: std_logic_vector(31 downto 0);
      reset_type: t_reset_type;
      decoder_mask: std_logic_vector(31 downto 0);
      access_type: t_access_type;
   end record;
   
   type t_axi4lite_multi_slave_reg_bank1_descr is record
      reg0: t_reg_descr;
      reg1: t_reg_descr;
      reg2: t_reg_descr;
      reg3: t_reg_descr;
   end record;

   
   constant axi4lite_multi_slave_reg_bank1_descr: t_axi4lite_multi_slave_reg_bank1_descr := (
      
      reg0 => (X"00000000",31, 0,X"00000000",async_reset,X"0000001c",rw),
      reg1 => (X"00000004",31, 0,X"00000000",async_reset,X"0000001c",rw),
      reg2 => (X"00000008",31, 0,X"00000000",async_reset,X"0000001c",rw),
      reg3 => (X"00000010",31, 0,X"00000000",async_reset,X"0000001c",rw)
   );

   --##########################################################################
   --
   -- Constants
   --
   --##########################################################################
   constant c_nof_register_blocks: integer := 1;
   constant c_nof_memory_blocks: integer := 0;
   constant c_total_nof_blocks: integer := c_nof_memory_blocks+c_nof_register_blocks;
   
   type t_ipb_multi_slave_reg_bank1_mosi_arr is array (0 to c_total_nof_blocks-1) of t_ipb_mosi;
   type t_ipb_multi_slave_reg_bank1_miso_arr is array (0 to c_total_nof_blocks-1) of t_ipb_miso;
   


   --##########################################################################
   --
   -- Functions
   --
   --##########################################################################
   function axi4lite_multi_slave_reg_bank1_decoder(descr: t_reg_descr; addr: std_logic_vector) return boolean;
   
   function axi4lite_multi_slave_reg_bank1_full_decoder(addr: std_logic_vector; en: std_logic) return t_axi4lite_multi_slave_reg_bank1_decoded;
   
   procedure axi4lite_multi_slave_reg_bank1_reset(signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1);
   procedure axi4lite_multi_slave_reg_bank1_default_decoded(signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1_decoded);
   procedure axi4lite_multi_slave_reg_bank1_write_reg(data: std_logic_vector; 
                                          signal multi_slave_reg_bank1_decoded: in t_axi4lite_multi_slave_reg_bank1_decoded;
                                          signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1);
   
   function axi4lite_multi_slave_reg_bank1_read_reg(signal multi_slave_reg_bank1_decoded: in t_axi4lite_multi_slave_reg_bank1_decoded;
                                        signal multi_slave_reg_bank1: t_axi4lite_multi_slave_reg_bank1) return std_logic_vector;
   
   function axi4lite_multi_slave_reg_bank1_demux(addr: std_logic_vector) return std_logic_vector;

end package;

package body axi4lite_multi_slave_reg_bank1_pkg is
   
   function axi4lite_multi_slave_reg_bank1_decoder(descr: t_reg_descr; addr: std_logic_vector) return boolean is
      variable ret: boolean:=true;
      variable bus_addr_i: std_logic_vector(addr'length-1 downto 0) := addr;
      variable mask_i: std_logic_vector(descr.decoder_mask'length-1 downto 0) := descr.decoder_mask;
      variable reg_addr_i: std_logic_vector(descr.offset'length-1 downto 0) := descr.offset;
   begin
      for n in 0 to bus_addr_i'length-1 loop
         if mask_i(n) = '1' and bus_addr_i(n) /= reg_addr_i(n) then
            ret := false;
         end if;
      end loop;
      return ret;
   end function;
   
   function axi4lite_multi_slave_reg_bank1_full_decoder(addr: std_logic_vector; en: std_logic) return t_axi4lite_multi_slave_reg_bank1_decoded is
      variable multi_slave_reg_bank1_decoded: t_axi4lite_multi_slave_reg_bank1_decoded;
   begin
         
      multi_slave_reg_bank1_decoded.reg0 := '0';
      if axi4lite_multi_slave_reg_bank1_decoder(axi4lite_multi_slave_reg_bank1_descr.reg0,addr) = true and en = '1' then
         multi_slave_reg_bank1_decoded.reg0 := '1';
      end if;
      
      multi_slave_reg_bank1_decoded.reg1 := '0';
      if axi4lite_multi_slave_reg_bank1_decoder(axi4lite_multi_slave_reg_bank1_descr.reg1,addr) = true and en = '1' then
         multi_slave_reg_bank1_decoded.reg1 := '1';
      end if;
      
      multi_slave_reg_bank1_decoded.reg2 := '0';
      if axi4lite_multi_slave_reg_bank1_decoder(axi4lite_multi_slave_reg_bank1_descr.reg2,addr) = true and en = '1' then
         multi_slave_reg_bank1_decoded.reg2 := '1';
      end if;
      
      multi_slave_reg_bank1_decoded.reg3 := '0';
      if axi4lite_multi_slave_reg_bank1_decoder(axi4lite_multi_slave_reg_bank1_descr.reg3,addr) = true and en = '1' then
         multi_slave_reg_bank1_decoded.reg3 := '1';
      end if;
            
      return multi_slave_reg_bank1_decoded;
   end function;
     
   procedure axi4lite_multi_slave_reg_bank1_reset(signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1) is
   begin
      multi_slave_reg_bank1.reg0 <= axi4lite_multi_slave_reg_bank1_descr.reg0.rst_val(31 downto 0);
      multi_slave_reg_bank1.reg1 <= axi4lite_multi_slave_reg_bank1_descr.reg1.rst_val(31 downto 0);
      multi_slave_reg_bank1.reg2 <= axi4lite_multi_slave_reg_bank1_descr.reg2.rst_val(31 downto 0);
      multi_slave_reg_bank1.reg3 <= axi4lite_multi_slave_reg_bank1_descr.reg3.rst_val(31 downto 0);
 
   end procedure;
   
   procedure axi4lite_multi_slave_reg_bank1_default_decoded(signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1_decoded) is
   begin
      
      multi_slave_reg_bank1.reg0 <= '0';
      multi_slave_reg_bank1.reg1 <= '0';
      multi_slave_reg_bank1.reg2 <= '0';
      multi_slave_reg_bank1.reg3 <= '0';

   end procedure;

   procedure axi4lite_multi_slave_reg_bank1_write_reg(data: std_logic_vector; 
                                          signal multi_slave_reg_bank1_decoded: in t_axi4lite_multi_slave_reg_bank1_decoded;
                                          signal multi_slave_reg_bank1: inout t_axi4lite_multi_slave_reg_bank1) is
   begin
      
          
      if multi_slave_reg_bank1_decoded.reg0 = '1' then
         multi_slave_reg_bank1.reg0 <= data(31 downto 0);
      end if;
      
      if multi_slave_reg_bank1_decoded.reg1 = '1' then
         multi_slave_reg_bank1.reg1 <= data(31 downto 0);
      end if;
      
      if multi_slave_reg_bank1_decoded.reg2 = '1' then
         multi_slave_reg_bank1.reg2 <= data(31 downto 0);
      end if;
      
      if multi_slave_reg_bank1_decoded.reg3 = '1' then
         multi_slave_reg_bank1.reg3 <= data(31 downto 0);
      end if;
      

   end procedure;
   
   function axi4lite_multi_slave_reg_bank1_read_reg(signal multi_slave_reg_bank1_decoded: in t_axi4lite_multi_slave_reg_bank1_decoded;
                                        signal multi_slave_reg_bank1: t_axi4lite_multi_slave_reg_bank1) return std_logic_vector is
      variable ret: std_logic_vector(31 downto 0);
   begin
      ret := (others=>'0');
            
      if multi_slave_reg_bank1_decoded.reg0 = '1' then
         ret(31 downto 0) := multi_slave_reg_bank1.reg0;
      end if;
      
      if multi_slave_reg_bank1_decoded.reg1 = '1' then
         ret(31 downto 0) := multi_slave_reg_bank1.reg1;
      end if;
      
      if multi_slave_reg_bank1_decoded.reg2 = '1' then
         ret(31 downto 0) := multi_slave_reg_bank1.reg2;
      end if;
      
      if multi_slave_reg_bank1_decoded.reg3 = '1' then
         ret(31 downto 0) := multi_slave_reg_bank1.reg3;
      end if;
      

      return ret;
   end function;
   
   function axi4lite_multi_slave_reg_bank1_demux(addr: std_logic_vector) return std_logic_vector is
      variable ret: std_logic_vector(c_total_nof_blocks-1 downto 0);
   begin
      ret := (others=>'0');
      if c_total_nof_blocks = 1 then
         ret := (others=>'1');
      else
      end if;
      return ret;
   end function;

end package body;
