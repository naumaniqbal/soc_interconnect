-- <l---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------l>
-- <h---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------h>
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library embedded_test_lib;
use embedded_test_lib.embedded_test_pkg.all;
use embedded_test_lib.axi4lite_multi_slave_ic_pkg.all;
use embedded_test_lib.axi4lite_multi_slave_mmap_pkg.all;


library axi4_lib;
use axi4_lib.axi4lite_pkg.all;
use axi4_lib.axi4s_pkg.all;

entity axi4lite_multi_slave_top is
    generic(
        g_fpga_vendor                       : string := "xilinx";
        g_fpga_family                       : string := "all";
        g_fifo_implementation               : string := "auto"        
    );
    port(
        axi4lite_aclk                       : in    std_logic;
        axi4lite_aresetn                    : in    std_logic;
        axi4lite_mosi                       : in    t_axi4lite_mosi;
        axi4lite_miso                       : out   t_axi4lite_miso
    );
end entity axi4lite_multi_slave_top;

architecture struct of axi4lite_multi_slave_top is


signal axi4lite_mosi_arr                    : t_axi4lite_mosi_arr(0 to c_axi4lite_mmap_nof_slave-1);
signal axi4lite_miso_arr                    : t_axi4lite_miso_arr(0 to c_axi4lite_mmap_nof_slave-1);


begin
    
    axi4lite_multi_slave_ic_inst : entity embedded_test_lib.axi4lite_multi_slave_ic
        port map(
            axi4lite_aclk                   => axi4lite_aclk,
            axi4lite_aresetn                => axi4lite_aresetn,
            axi4lite_mosi                   => axi4lite_mosi,
            axi4lite_mosi_arr               => axi4lite_mosi_arr,
            axi4lite_miso_arr               => axi4lite_miso_arr,
            axi4lite_miso                   => axi4lite_miso
        );
        reg_block_inst_0 : entity embedded_test_lib.axi4lite_multi_slave_reg_bank0
    	port map(
			axi4lite_aclk 					=>  axi4lite_aclk,
			axi4lite_aresetn				=>  axi4lite_aresetn,
			axi4lite_mosi      				=>  axi4lite_mosi_arr(axi4lite_mmap_get_id(id_mem_a)),
        	axi4lite_miso       			=>  axi4lite_miso_arr(axi4lite_mmap_get_id(id_mem_a))

	);
	
        reg_block_inst_1 : entity embedded_test_lib.axi4lite_multi_slave_reg_bank1
    	port map(
			axi4lite_aclk 				    =>  axi4lite_aclk,
			axi4lite_aresetn 				=>  axi4lite_aresetn,
			axi4lite_mosi      				=>  axi4lite_mosi_arr(axi4lite_mmap_get_id(id_mem_b)),
        	axi4lite_miso       			=>  axi4lite_miso_arr(axi4lite_mmap_get_id(id_mem_b))
	);
    
end architecture struct;

