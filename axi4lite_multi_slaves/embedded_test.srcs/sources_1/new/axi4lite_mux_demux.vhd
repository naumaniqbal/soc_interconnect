library ieee;
use ieee.std_logic_1164.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;
library work;
use work.axi4lite_emb_packet_checker_pkg.all;
     
entity axi4lite_emb_packet_checker_muxdemux is
   port(
      axi4lite_aclk : in std_logic; 
      axi4lite_aresetn : in std_logic; 
      
      ipb_mosi : in t_ipb_mosi;
      ipb_miso : out t_ipb_miso;
      
      ipb_mosi_arr : out t_ipb_emb_packet_checker_mosi_arr;
      ipb_miso_arr : in t_ipb_emb_packet_checker_miso_arr
   );
end entity;     

architecture axi4lite_emb_packet_checker_muxdemux_a of axi4lite_emb_packet_checker_muxdemux is 
   
   constant c_block_start_index: integer := c_nof_register_blocks;
   
   signal ipb_mosi_arr_i   : t_ipb_emb_packet_checker_mosi_arr;
   
begin   
   --
   -- only one memory block or registers block, no need to mux/demux
   --
   gen_0: if c_total_nof_blocks = 1 generate
      ipb_mosi_arr_i(0) <= ipb_mosi;
      ipb_miso <= ipb_miso_arr(0);
   end generate;
   --
   -- more than one block
   --
   gen_1: if c_total_nof_blocks > 1 generate      
      --demux process
      demux_p: process(ipb_miso_arr,ipb_mosi_arr_i,ipb_mosi)
         variable hit: std_logic_vector(c_total_nof_blocks-1 downto 0);
      begin
         hit := axi4lite_emb_packet_checker_demux(ipb_mosi.addr);
         for n in 0 to c_total_nof_blocks-1 loop
            ipb_mosi_arr_i(n).wreq <= ipb_mosi.wreq and hit(n);
            ipb_mosi_arr_i(n).rreq <= ipb_mosi.rreq and hit(n);
            ipb_mosi_arr_i(n).addr <= ipb_mosi.addr;
            ipb_mosi_arr_i(n).wdat <= ipb_mosi.wdat;
         end loop;
      end process;
      --mux process
      mux_p: process(ipb_miso_arr,ipb_mosi_arr_i)
      begin
         ipb_miso.rack <= '0';
         ipb_miso.wack <= '0';
         ipb_miso.rdat <= (others=>'-');
         for n in 0 to c_total_nof_blocks-1 loop
            if ipb_miso_arr(n).wack = '1' and ipb_mosi_arr_i(n).wreq = '1' then
               ipb_miso.wack <= '1';
            end if;
            if ipb_miso_arr(n).rack = '1' and ipb_mosi_arr_i(n).rreq = '1' then
               ipb_miso.rack <= '1';
               ipb_miso.rdat <= ipb_miso_arr(n).rdat;
            end if;
         end loop;
      end process;
   end generate;   
   
   ipb_mosi_arr <= ipb_mosi_arr_i;
      
end architecture;

